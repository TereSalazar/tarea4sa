var express = require("express")
var app = express()
var fs = require("fs")
var clientes = require("./cliente")

app.get("/listarCliente", function(req, res) {
  fs.readFile(__dirname + "/" + "cliente.json", "utf8", function(err, data) {
    console.log(data)
    res.end(data)
  })
})

/*
 *   Devuelve datos del cliente recibe parámetro id
 */
app.get("/cliente/:id", function(req, res) {
  fs.readFile(__dirname + "/" + "clientes.json", "utf8", function(err, data) {
    var users = JSON.parse(data)
    var user = users["cliente" + req.params.id]
    console.log(user)
    res.end(JSON.stringify(user))
    console.log("Usuario en sesion")
  })
})

/*
 *   Configuración del puerto
 */
var server = app.listen(8000, function() {
  var host = server.address().address
  var port = server.address().port
  console.log("Servidor escuchando", host, port)
})
