var express = require("express")
var app = express()
var fs = require("fs")
var bodyParser = require("body-parser")
const request = require("request")
const axios = require("axios")

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "500mb"
  })
)

/*
 *   Recibe parámetros idPlatillo, idCliente y dirección
 *   devuelve datos del nombreCliente, nombrePlatillo, 
 *   dirección y nombreRepartidor asignado.
 */
app.post("/pedido", function(req, res) {
  var idC = req.body.idCliente
  var idP = req.body.idPlatillo
  var direc = req.body.direccion

  axios.get("http://localhost:8000/cliente/" + idC).then(function(response) {
    //console.log(response.data);

    axios.get("http://localhost:8002/repartidor/1").then(function(response2) {
      axios
        .post("http://localhost:8001/platillo", { idPlatillo: idP,nombre: response.data.nombre})
        .then(function(response3) {
          //console.log(response3);
          datos = {
            nombreCliente: response.data.nombre,
            Platillo: response3.data,
            Direccion: direc,
            Repartidor: response2.data.nombre
          }
          console.log(datos)
          res.send(datos)
        })
    })
  })
})

/*
 *   Configuración del puerto
 */
var server = app.listen(8004, function() {
  var host = server.address().address
  var port = server.address().port
  console.log("Servidor escuchando", host, port);
})
