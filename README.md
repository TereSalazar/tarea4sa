# TABLA DE CONTENIDO

* Información General
* Tecnología
* Instalación
* Video
* Test

## Información general

Simulación de servicios.

* Solicitud de comida por parte del cliente
* Recepcion de órdenes por parte del restaurante
* Servicio de entrega por repartidor

## Tecnología

* Node js version 12.14.1

## Instalación

* Descargar o clonar proyecto
* Dirigirse en consola a la carpeta donde se descargó o clonó el proyecto
* Escribir en consola node index.js para ejecutar el proyecto

## Video Demo

*[tarea2](https://youtu.be/0cdYuRQeDcY)
*[tarea3](https://youtu.be/Wf_v1Q5hgM4)
*[netlify](https://hardcore-heyrovsky-80a04d.netlify.com)
