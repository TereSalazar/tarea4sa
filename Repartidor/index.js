var express = require("express")
var app = express()
var fs = require("fs")

app.get("/listarRepartidor", function(req, res) {
  fs.readFile(__dirname + "/" + "repartidor.json", "utf8", function(err, data) {
    console.log(data)
    res.end(data)
  })
})

/*
 *   Devuelve datos del repartidor recibe parámetro id
 */
app.get("/repartidor/:id", function(req, res) {
  let arch = fs.readFileSync(__dirname + "/" + "repartidor.json", "utf8")
  let data = JSON.parse(arch)

  let repart = data.find(valor => valor.idRepartidor == req.params.id)
  console.log("******************Repartidor Asignado****************")
  console.log(repart)
  res.end(JSON.stringify(repart))
})

/*
 *   Configuración del puerto
 */
var server = app.listen(8002, function() {
  var host = server.address().address
  var port = server.address().port
  console.log("Servidor escuchando", host, port,"\n")
})
