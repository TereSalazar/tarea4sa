# Servicio Restaurante

Servicio encargado de recibir datos de la orden y asignar repartidor, este funciona en el puerto localhost:8001/.

## Recursos

## /orden

Este servicio utiliza un POST el cual recibe los siguientes datos:


[
~~~        
        idPlatillo: numero,
        NombreCliente: valor
~~~
]