var express = require("express")
var app = express()
var fs = require("fs")
var bodyParser = require("body-parser")
const request = require("request")
const axios = require("axios")

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "500mb"
  })
)

/*
 *   Recibe parámetros idPlatillo y nombre del cliente 
 *   devuelve datos del nombre del platillo
 */
app.post("/platillo", function(req, res) {
  var idP = req.body.idPlatillo
  var nombreC = req.body.nombre
  var plat = datoPlatillo(idP)

  datos = {
    nombreCliente: nombreC,
    Platillo: plat
  }
  console.log("***********Datos Orden********************")
  console.log(datos)
  res.send(plat)
})

/*
 *   Función que retorna nombre del platillo
 */
function datoPlatillo(idP) {
  let arch = fs.readFileSync(__dirname + "/" + "platillo.json", "utf8")
  let data = JSON.parse(arch)

  let plato = data.find(valor => valor.idPlatillo == idP)
  //console.log(plato.nombre)
  return plato.nombre
}

/*
 *   Configuración del puerto
 */
var server = app.listen(8001, function() {
  var host = server.address().address
  var port = server.address().port
  console.log("Servidor escuchando", host, port,"\n")
})
