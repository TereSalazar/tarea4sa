const gulp = require('gulp')
const zip = require('gulp-zip')
const fileindex = require('gulp-fileindex')

gulp.task('default', function(){
    return gulp.src('./**/*')
        .pipe(zip('dist.zip'))
        .pipe(gulp.dest('dist'))
})